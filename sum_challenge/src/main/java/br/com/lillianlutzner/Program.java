package br.com.lillianlutzner;

import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Coloque o primeiro valor: ");
		int a = sc.nextInt();
		System.out.println("Coloque o segundo valor: ");
		int b = sc.nextInt();

		int subt = sum(a, b);
		
		if (a > b) {
			subt = sum(a, b);
		} else {
			subt = sum(b, a);
		}
		
		showResult(subt);

	}

	public static int sum(int x, int y) {
		int subtracao = 0;
			while (x > y) {
				y += 1;
				subtracao++;
			}
			return subtracao;
		} 
		

	public static void showResult(int value) {
		System.out.println("O resultado da subtração é " + value);
	}

}
